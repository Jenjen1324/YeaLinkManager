<?php
/**
 * Created by PhpStorm.
 * User: jvogler
 * Date: 12.03.2019
 * Time: 15:02
 */

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Tools\Console\ConsoleRunner;

/** @var \DI\Container $container */
$container = require_once __DIR__ . '/bootstrap.php';


$em = $container->get(EntityManager::class);
return ConsoleRunner::createHelperSet($em);