<?php
/**
 * Created by PhpStorm.
 * User: jvogler
 * Date: 19.03.2019
 * Time: 09:45
 */

namespace YeaLinkManager\Handlers\View;


use DI\Annotation\Inject;
use Doctrine\ORM\EntityManager;
use League\Plates\Engine;
use YeaLinkManager\Entities\PBPerson;
use YeaLinkManager\HTTP\Response;

class ListController {


    /**
     * @var EntityManager
     * @Inject()
     */
    private $abc;

    public function getList(Response $response, EntityManager $em, Engine $engine) {

        /** @var PBPerson[] $persons */
        $persons = $em->getRepository(PBPerson::class)->findAll();

        $data = [];

        foreach ($persons as $person) {

            $entry = [
                'name' => $person->getName(),
                'numbers' => [],
            ];
            $numbers = $person->getPbNumbers();

            foreach ($numbers as $number) {
                $entry['numbers'][] = $number->getPhoneNumber();
            }

            $data[] = $entry;
        }

        $response->html($engine->make('html/list')->render(['data' => $data]));
    }


}