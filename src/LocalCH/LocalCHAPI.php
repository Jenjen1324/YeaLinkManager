<?php
/**
 * Created by PhpStorm.
 * User: jvogler
 * Date: 13.03.2019
 * Time: 11:00
 */

namespace YeaLinkManager\LocalCH;


use DI\Annotation\Inject;

class LocalCHAPI {

    const BASE_URL = "https://tel.search.ch/api/";

    /**
     * @var \Psr\Log\LoggerInterface
     * @Inject()
     */
    private $log;

    public function lookup($phoneNumber) {
        return $this->query(['was' => $phoneNumber]);
    }

    private function query(array $queryParams) {

        $url = self::BASE_URL . '?' . http_build_query($queryParams);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        curl_setopt($ch, CURLOPT_FAILONERROR, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        $output = curl_exec($ch);

        curl_close($ch);

        $this->log->debug($output);

        $xml = new \SimpleXMLElement($output);

        if (isset($xml->entry[0])) {
            $xmlEntry = $xml->entry[0];
            $entry = new LocalCHEntry();

            $entry->id = (string) $xmlEntry->id;
            $entry->content = (string) $xmlEntry->content;
            $entry->title = (string) $xmlEntry->title;
            $entry->updated = (string) $xmlEntry->updated;
            $entry->published = (string) $xmlEntry->published;

            return $entry;
        }

        return null;


    }

}