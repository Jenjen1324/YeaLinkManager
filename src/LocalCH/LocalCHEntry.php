<?php
/**
 * Created by PhpStorm.
 * User: jvogler
 * Date: 13.03.2019
 * Time: 11:07
 */

namespace YeaLinkManager\LocalCH;


class LocalCHEntry {

    public $id;
    public $updated;
    public $published;
    public $title;
    public $content;


}