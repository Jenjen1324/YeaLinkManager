<?php
/**
 * Created by PhpStorm.
 * User: jvogler
 * Date: 12.03.2019
 * Time: 19:04
 */

namespace YeaLinkManager;


use Doctrine\ORM\EntityManager;
use FastRoute\RouteCollector;
use YeaLinkManager\Entities\PBNumber;
use YeaLinkManager\Handlers\View\ListController;
use YeaLinkManager\HTTP\Response;
use function array_map;

class Routes {

    public static function applyRoutes(RouteCollector $r) {
        $r->addRoute('GET', '/phone/onIncomingCall', [PhoneSubmitHandler::class, 'onIncomingCall']);
        $r->addRoute('GET', '/', [ListController::class, 'getList']);
        $r->addRoute('GET', '/test', [ListController::class, 'test']);

        $r->addRoute('GET', '/updateTest', function (Response $response, EntityManager $em) {

            Test::updateField($em, PBNumber::class, 'pbPerson', 1, 3);
            $em->flush();

            $persons = $em->getRepository(PBNumber::class)->findAll();

            $response->text(print_r(array_map(function ($v) {
                return [
                    $v->getId(),
                    $v->getPbPerson()->getId(),
                    $v->getPbPerson()->getName()
                ];

            }, $persons), true));
        });
    }

}