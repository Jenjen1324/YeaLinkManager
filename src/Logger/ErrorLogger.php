<?php
/**
 * Created by PhpStorm.
 * User: jvogler
 * Date: 13.03.2019
 * Time: 10:28
 */

namespace YeaLinkManager\Logger;


use Psr\Log\AbstractLogger;

class ErrorLogger extends AbstractLogger {

    /**
     * Logs with an arbitrary level.
     *
     * @param mixed  $level
     * @param string $message
     * @param array  $context
     *
     * @return void
     */
    public function log($level, $message, array $context = array()) {
        $msg = "[{$level}] {$message}";

        if (count($context) > 0) {
            $msg .= " " . LogHelper::dumpArray($context);
        }

        error_log($msg);
    }
}