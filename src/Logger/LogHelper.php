<?php
/**
 * Created by PhpStorm.
 * User: jvogler
 * Date: 13.03.2019
 * Time: 10:16
 */

namespace YeaLinkManager\Logger;


use stdClass;

class LogHelper {



    public static function dumpValue($val, $indent = 0) {
        $str = '';

        if (is_object($val)) {
            if ($val instanceof \Exception) {
                return self::jTraceEx($val);
            }

            $str .= '[object] ';
            $str .= get_class($val);

            if (method_exists($val, 'getId')) {
                $str .= '(' . $val->getId() . ')';
            }
            else if (method_exists($val, '__toString()')) {
                /** @noinspection PhpUndefinedMethodInspection */
                $str .= '"' . $val->__toString() . '"';
            }
            else if ($val instanceof stdClass) {
                $str .= ' -> members = ' . self::dumpArray((array)$val, $indent);
            }
        }
        else if (is_array($val)) {
            $str .= self::dumpArray($val, $indent);
        }
        else if (is_string($val)) {
            $str .= '[string] "';
            $str .= $val;
            $str .= '"';
        }
        else if (is_int($val)) {
            $str .= '[int] ' . $val;
        }
        else if (is_bool($val)) {
            $str .= '[boolean] ' . ($val ? 'true' : 'false');
        }
        else {
            $str .= '[' . gettype($val) . '] ' . (string)$val;
        }

        return $str;
    }

    public static function dumpArray(array $array, $indent = 0) {
        $str = str_repeat(' ', $indent);
        $str .= "array (\n";
        $indent += 4;
        foreach ($array as $key => $val) {

            $str .= str_repeat(' ', $indent);
            if (is_int($key)) {
                $str .= "{$key} => \t";
            }
            else {
                $str .= "'{$key}' => \t";
            }

            $str .= self::dumpValue($val, $indent);

            $str .= "\n";

        }

        $indent -= 4;
        $str .= str_repeat(' ', $indent);
        $str .= ')' . "\n";
        return $str;
    }


    /**
     * jTraceEx() - provide a Java style exception trace
     *
     * Source: https://secure.php.net/manual/en/exception.gettraceasstring.php
     *
     * @param \Exception $e
     * @param            $seen - array passed to recursive calls to accumulate trace lines already seen
     *                         leave as NULL when calling this function
     *
     * @return string
     */
    public static function jTraceEx($e, $seen = null) {
        $starter = $seen ? 'Caused by: ' : '';
        $result = array();
        if (!$seen) {
            $seen = array();
        }
        $trace = $e->getTrace();
        $prev = $e->getPrevious();
        $result[] = sprintf('%s%s: %s', $starter, get_class($e), $e->getMessage());
        $file = $e->getFile();
        $line = $e->getLine();
        while (true) {
            $current = "$file:$line";
            //if (is_array($seen) && in_array($current, $seen)) {
            //    $result[] = sprintf(' ... %d more', count($trace) + 1);
            //    break;
            //}
            $result[] = sprintf(' at %s%s%s(%s%s%s)',
                                count($trace) && array_key_exists('class', $trace[0]) ? str_replace('\\', '.', $trace[0]['class']) : '',
                                count($trace) && array_key_exists('class', $trace[0]) && array_key_exists('function', $trace[0]) ? '.' : '',
                                count($trace) && array_key_exists('function', $trace[0]) ? str_replace('\\', '.', $trace[0]['function']) : '(main)',
                                $line === null ? $file : basename($file),
                                $line === null ? '' : ':',
                                $line === null ? '' : $line);
            if (is_array($seen)) {
                $seen[] = "$file:$line";
            }
            if (!count($trace)) {
                break;
            }
            $file = array_key_exists('file', $trace[0]) ? $trace[0]['file'] : 'Unknown Source';
            $line = array_key_exists('file', $trace[0]) && array_key_exists('line', $trace[0]) && $trace[0]['line'] ? $trace[0]['line'] : null;
            array_shift($trace);
        }
        $result = implode("\n", $result);
        if ($prev) {
            $result .= "\n" . self::jTraceEx($prev, $seen);
        }

        return $result;
    }

}