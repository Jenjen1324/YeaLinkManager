<?php
/**
 * Created by PhpStorm.
 * User: jvogler
 * Date: 13.03.2019
 * Time: 10:46
 */

namespace YeaLinkManager\Logger;


use Psr\Log\AbstractLogger;
use Psr\Log\LoggerInterface;

class FormattedLogWrapper extends AbstractLogger {

    private $logger;

    public function __construct(LoggerInterface $logger) {
        $this->logger = $logger;
    }

    /**
     * Logs with an arbitrary level.
     *
     * @param mixed  $level
     * @param string $message
     * @param array  $context
     *
     * @return void
     */
    public function log($level, $message, array $context = array()) {
        if (count($context) > 0) {
            $message .= " " . LogHelper::dumpArray($context);
        }

        $this->logger->log($level, $message);
    }
}