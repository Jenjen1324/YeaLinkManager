<?php
/**
 * Created by PhpStorm.
 * User: jvogler
 * Date: 12.03.2019
 * Time: 19:07
 */

namespace YeaLinkManager;


use DI\Annotation\Inject;
use Doctrine\ORM\EntityManager;
use YeaLinkManager\Entities\PBNumber;
use YeaLinkManager\Entities\PBPerson;
use YeaLinkManager\HTTP\Request;
use YeaLinkManager\HTTP\Response;
use YeaLinkManager\LocalCH\LocalCHAPI;

class PhoneSubmitHandler {

    /**
     * @var \Psr\Log\LoggerInterface
     * @Inject
     */
    private $log;

    /// http://10.0.6.10/phone/onIncomingCall?user=$active_user&remote=$remote&callerID=$callerID
    public function onIncomingCall(EntityManager $em, Request $request, Response $response, Scheduler $scheduler, LocalCHAPI $api) {
        $params = $request->getQueryParams();

        $data = [
            'user'     => $params->string('user'),
            'remote'   => $params->string('remote'),
            'callerID' => $params->string('callerID'),
        ];


        // <sip:123123@wat.com>
        $matches = [];
        preg_match("/sip:(.*)@.*/", $data['remote'], $matches);


        $data['phoneNumber'] = $matches[1];

        $this->log->info('Incoming Call', $data);

        $response->text('Ok');


        $scheduler->scheduleAfterRequest(function () use ($em, $data, $api) {

            // <sip:123123@wat.com>

            /** @var PBNumber $pbNumber */
            $pbNumber = $em->getRepository(PBNumber::class)->findOneBy(['phoneNumber' => $data['phoneNumber']]);

            if ($pbNumber instanceof PBNumber) {
                $this->log->info('Number found: ' . $pbNumber->getPbPerson()->getName());
            }
            else {
                $this->log->info('New Number, saving and doing lookup...');

                $pbNumber = new PBNumber();
                $pbNumber->setPhoneNumber($data['phoneNumber']);

                $pbPerson = new PBPerson();
                $pbNumber->setPbPerson($pbPerson);


                if (substr($data['phoneNumber'], 0, 1) === '0') {
                    $entry = $api->lookup($data['phoneNumber']);
                } else {
                    $this->log->warning('Invalid number, skipping lookup');
                    $entry = null;
                }

                $pbPerson->setHadLookup(true);

                if ($entry) {
                    $this->log->info('Number found: ' . $entry->title);
                    $pbPerson->setLookupID($entry->id);
                    $pbPerson->setName($entry->title);
                    $pbPerson->setPhoneBookText($entry->content);
                }
                else {
                    $this->log->info('No number found');
                    $pbPerson->setName('Unknown');
                }

                $em->persist($pbPerson);
                $em->persist($pbNumber);
                $em->flush();
            }

            // TODO: Implement Call Log

        });
    }

}