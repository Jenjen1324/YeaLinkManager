<?php
/**
 * Created by PhpStorm.
 * User: jvogler
 * Date: 12.03.2019
 * Time: 19:09
 */

namespace YeaLinkManager\HTTP;


use DateTime;

class RequestRepository {

    /**
     * @var array
     */
    protected $dataArray;

    public function __construct($dataArray) {
        $this->dataArray = $dataArray;
    }

    public function string(string $key, $default = null) {
        return isset($this->dataArray[$key]) ? (string) filter_var($this->dataArray[$key], FILTER_UNSAFE_RAW) : $default;
    }

    /**
     * @param string $key
     * @param string $allowedChars
     * @param bool   $allowNumbers
     * @param bool   $toLowerCase
     *
     * @return mixed
     */
    public function name(string $key, string $allowedChars = "", bool $allowNumbers = true, bool $toLowerCase = false): string {
        $filtered = $this->string($key);

        if ($toLowerCase) {
            $filtered = strtolower($filtered);
        }

        // Escape special characters for the preg_replace() below.  In character classes only the characters ^, -, ] and / have a special meanings and therefore must be escaped
        $allowedChars = preg_replace("/([\\^\\-\\]\\/])/", "\\\\$1", $allowedChars);
        if ($allowNumbers) {
            $allowedChars .= "0-9";
        }
        // It's ok to allow A-Z here, because we already added to_lowercase
        $allowedChars = "/[^a-zA-Z" . $allowedChars . ']/';

        // Replace found chars with nothing
        return preg_replace($allowedChars, '', $filtered);
    }

    public function int(string $key) {
        return (int) filter_var($this->dataArray[$key], FILTER_SANITIZE_NUMBER_INT);
    }

    public function float(string $key): float {
        return (float) filter_var($this->dataArray[$key], FILTER_SANITIZE_NUMBER_FLOAT);
    }

    public function email(string $key): string {
        return (string) filter_var($this->dataArray[$key], FILTER_SANITIZE_EMAIL);
    }

    public function date(string $key, bool $includeTime = false) {
        $value = $this->string($key);

        if ($value) {
            $format = $includeTime ? 'd.m.Y H:i' : '!' . 'd.m.Y';
            $dateTime = DateTime::createFromFormat($format, $value);
            return ($dateTime === false) ? null : $dateTime;
        }
        else {
            return null;
        }
    }

    public function array(string $key): array {
        return filter_var($this->dataArray[$key], FILTER_REQUIRE_ARRAY);
    }

}