<?php
/**
 * Created by PhpStorm.
 * User: jvogler
 * Date: 12.03.2019
 * Time: 19:09
 */

namespace YeaLinkManager\HTTP;


class Request {
    const METHOD_GET = 'GET';
    const METHOD_POST = 'POST';
    const METHOD_PUT = 'PUT';
    // etc...

    /**
     * @var string
     */
    protected $method;

    /**
     * @var string
     */
    protected $uri;

    /**
     * @var RequestRepository
     */
    protected $urlParams;

    /**
     * @var RequestRepository
     */
    protected $queryParams;

    /**
     * @var RequestRepository
     */
    protected $postParams;

    /**
     * Request constructor.
     *
     * @param string $method
     * @param string $uri
     */
    public function __construct(string $method, string $uri)
    {
        $this->method = $method;
        $this->uri = $uri;
    }

    public static function getHTTPRequest()
    {
        $method = $_SERVER['REQUEST_METHOD'];
        $uri = $_SERVER['REQUEST_URI'];


        // Strip query string (?foo=bar) and decode URI
        if (false !== $pos = strpos($uri, '?')) {
            $uri = substr($uri, 0, $pos);
        }
        $uri = rawurldecode($uri);

        $requestObj = new Request($method, $uri);
        $requestObj->queryParams = new RequestRepository($_GET);
        $requestObj->postParams = new RequestRepository($_POST);

        return $requestObj;
    }

    /**
     * @return string
     */
    public function getMethod(): string
    {
        return $this->method;
    }

    /**
     * @return string
     */
    public function getUri(): string
    {
        return $this->uri;
    }

    public function getQueryParams(): RequestRepository
    {
        return $this->queryParams;
    }

    public function getJSONPost(): RequestRepository
    {
        $data = json_decode(file_get_contents('php://input'), true);
        return new RequestRepository($data);
    }

    public function getPost(): RequestRepository
    {
        return $this->postParams;
    }

    public function getUrlParams(): RequestRepository
    {
        return $this->urlParams;
    }


    public function setUrlParams($dataArray)
    {
        return $this->urlParams = new RequestRepository($dataArray);
    }


}