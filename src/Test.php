<?php


namespace YeaLinkManager;


use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Mapping\MappingException;
use Exception;
use function method_exists;
use function ucfirst;

class Test {


    public static function updateField(EntityManager $em, string $tableClass, string $fieldName, $id, $value) {

        try {
            $mapping = $em->getClassMetadata($tableClass)->getAssociationMapping($fieldName);
            if (isset($mapping['targetEntity'])) {
                $value = $em->getReference($mapping['targetEntity'], $value);
            }
        } catch (MappingException $e) {
            // no mapping
        }

        $entity = $em->getRepository($tableClass)->find($id);
        $setter = 'set' . ucfirst($fieldName);

        if (!method_exists($entity, $setter)) {
            throw new Exception("No setter named: $setter");
        }

        $entity->$setter($value);
    }

}