<?php
/**
 * Created by PhpStorm.
 * User: jvogler
 * Date: 12.03.2019
 * Time: 14:37
 */

namespace YeaLinkManager\Entities;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class PBNumber
 *
 * @package YeaLinkManager\Entities
 * @ORM\Entity
 */
class PBNumber {

    /**
     * @var int
     * @ORM\Id
     * @ORM\Column(nullable=false, type="integer")
     * @ORM\GeneratedValue
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(nullable=false, length=255, type="string")
     */
    private $phoneNumber;

    /**
     * @var \YeaLinkManager\Entities\PBPerson
     * @ORM\ManyToOne(targetEntity="PBPerson", inversedBy="pbNumbers")
     */
    private $pbPerson;

    /**
     * @return int
     */
    public function getId(): int {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getPhoneNumber(): string {
        return $this->phoneNumber;
    }

    /**
     * @param string $phoneNumber
     *
     * @return PBNumber
     */
    public function setPhoneNumber(string $phoneNumber): PBNumber {
        $this->phoneNumber = $phoneNumber;
        return $this;
    }

    /**
     * @return \YeaLinkManager\Entities\PBPerson
     */
    public function getPbPerson(): PBPerson {
        return $this->pbPerson;
    }

    /**
     * @param \YeaLinkManager\Entities\PBPerson $pbPerson
     *
     * @return PBNumber
     */
    public function setPbPerson(PBPerson $pbPerson): PBNumber {
        $this->pbPerson = $pbPerson;
        return $this;
    }
}