<?php
/**
 * Created by PhpStorm.
 * User: jvogler
 * Date: 12.03.2019
 * Time: 14:37
 */

namespace YeaLinkManager\Entities;

use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class PBPerson
 *
 * @package YeaLinkManager\Entities
 * @ORM\Entity
 */
class PBPerson {

    /**
     * @var int
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(type="string", length=255, nullable=false)
     */
    private $name = 'Pending Lookup...';

    /**
     * @var string
     * @ORM\Column(type="text", nullable=false)
     */
    private $phoneBookText = '';

    /**
     * @var bool
     * @ORM\Column(type="boolean")
     */
    private $hadLookup = false;

    /**
     * @var string
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $lookupID = null;

    /**
     * @var PBNumber[]|Collection
     * @ORM\OneToMany(targetEntity="PBNumber", mappedBy="pbPerson")
     */
    private $pbNumbers;

    /**
     * @return int
     */
    public function getId(): int {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): string {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return PBPerson
     */
    public function setName(string $name): PBPerson {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getPhoneBookText(): string {
        return $this->phoneBookText;
    }

    /**
     * @param string $phoneBookText
     *
     * @return PBPerson
     */
    public function setPhoneBookText(string $phoneBookText): PBPerson {
        $this->phoneBookText = $phoneBookText;
        return $this;
    }

    /**
     * @return bool
     */
    public function isHadLookup(): bool {
        return $this->hadLookup;
    }

    /**
     * @param bool $hadLookup
     *
     * @return PBPerson
     */
    public function setHadLookup(bool $hadLookup): PBPerson {
        $this->hadLookup = $hadLookup;
        return $this;
    }

    /**
     * @return \Doctrine\Common\Collections\Collection|\YeaLinkManager\Entities\PBNumber[]
     */
    public function getPbNumbers() {
        return $this->pbNumbers;
    }

    /**
     * @param \Doctrine\Common\Collections\Collection|\YeaLinkManager\Entities\PBNumber[] $pbNumbers
     *
     * @return PBPerson
     */
    public function setPbNumbers($pbNumbers) {
        $this->pbNumbers = $pbNumbers;
        return $this;
    }

    /**
     * @return string
     */
    public function getLookupID(): string {
        return $this->lookupID;
    }

    /**
     * @param string $lookupID
     *
     * @return PBPerson
     */
    public function setLookupID(string $lookupID): PBPerson {
        $this->lookupID = $lookupID;
        return $this;
    }

}