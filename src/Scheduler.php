<?php
/**
 * Created by PhpStorm.
 * User: jvogler
 * Date: 13.03.2019
 * Time: 11:35
 */

namespace YeaLinkManager;


class Scheduler {

    /**
     * @var \Closure[]
     */
    private $tasks = [];

    public function scheduleAfterRequest(callable $callable) {
        $this->tasks[] = $callable;
    }

    public function run() {
        foreach ($this->tasks as $task) {
            $task();
        }
    }

}