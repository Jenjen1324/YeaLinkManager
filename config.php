<?php
/**
 * Created by PhpStorm.
 * User: jvogler
 * Date: 12.03.2019
 * Time: 14:20
 */

class Config {

    public $dbParams = [
        'driver' => 'pdo_sqlite',
        'path'   => __DIR__ . '/database.sqlite3',
    ];

    public $devMode = true;

    public $urlPrefix = '/jvogler/projects/yealinkmanager/web';

}