<?php
/**
 * Created by PhpStorm.
 * User: jvogler
 * Date: 12.03.2019
 * Time: 19:03
 */

use YeaLinkManager\HTTP\Request;
use YeaLinkManager\HTTP\Response;
use YeaLinkManager\Scheduler;

/** @var \DI\Container $container */
$container = require __DIR__ . '/../bootstrap.php';
$config = $container->get(Config::class);


$dispatcher = FastRoute\simpleDispatcher([\YeaLinkManager\Routes::class, 'applyRoutes']);

// Fetch method and URI from somewhere
$httpMethod = $_SERVER['REQUEST_METHOD'];
$uri = $_SERVER['REQUEST_URI'];

// Strip query string (?foo=bar) and decode URI
if (false !== $pos = strpos($uri, '?')) {
    $uri = substr($uri, 0, $pos);
}
$uri = rawurldecode($uri);

if (substr($uri, 0, strlen($config->urlPrefix)) == $config->urlPrefix) {
    $uri = substr($uri, strlen($config->urlPrefix));
}

$scheduler = $container->get(Scheduler::class);
$request = Request::getHTTPRequest();
$response = new Response();

$logger = $container->get(\Psr\Log\LoggerInterface::class);
$logger->info("{$httpMethod}: {$uri}");

$routeInfo = $dispatcher->dispatch($httpMethod, $uri);
switch ($routeInfo[0]) {
    case FastRoute\Dispatcher::NOT_FOUND:
        $response->code(404);
        $response->text("404 - Not found\n{$httpMethod}: {$uri}");
        break;
    case FastRoute\Dispatcher::METHOD_NOT_ALLOWED:
        $response->code(405);
        $response->text('405 - Method Not Allowed');
        $allowedMethods = $routeInfo[1];
        break;
    case FastRoute\Dispatcher::FOUND:
        $handler = $routeInfo[1];
        $vars = $routeInfo[2];

        $callable = $handler;

        $container->set(Request::class, $request);
        $container->set(Response::class, $response);

        if (is_array($handler)) {
            $class = $handler[0];
            $instance = $container->make($class);

            $callable = [$class, $handler[1]];
        }

        $container->call($callable);

        break;
}

try {
    $response->sendHeaders();
    echo $response->getPayload();

    ignore_user_abort(true);
    ob_flush();
    flush();

    $scheduler->run();

} catch (Exception $e) {
    /** @var \Psr\Log\LoggerInterface $logger */
    $logger->error($e->getMessage());
    $logger->error($e->getTraceAsString());
}