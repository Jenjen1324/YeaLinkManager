<?php
/**
 * Created by PhpStorm.
 * User: jvogler
 * Date: 13.03.2019
 * Time: 16:40
 */


$container = require_once __DIR__ . '/../bootstrap.php';

$classLoader = new \Composer\Autoload\ClassLoader();
$classLoader->addPsr4("YeaLinkManager\\", __DIR__, true);
$classLoader->register();