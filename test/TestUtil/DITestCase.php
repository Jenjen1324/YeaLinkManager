<?php
/**
 * Created by PhpStorm.
 * User: jvogler
 * Date: 13.03.2019
 * Time: 16:41
 */

namespace YeaLinkManager\TestUtil;


use PHPUnit\Framework\TestCase;

class DITestCase extends TestCase {

    /**
     * @var \DI\Container
     */
    protected $di;

    protected function setUp(): void {
        global $container;
        $this->di = $container;

        $container->injectOn($this);

        parent::setUp();
    }


}