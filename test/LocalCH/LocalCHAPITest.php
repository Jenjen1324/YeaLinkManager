<?php
/**
 * Created by PhpStorm.
 * User: jvogler
 * Date: 13.03.2019
 * Time: 11:17
 */

namespace YeaLinkManager\LocalCH;


use YeaLinkManager\TestUtil\DITestCase;

class LocalCHAPITest extends DITestCase {

    public function testAPI() {

        $api = $this->di->get(LocalCHAPI::class);
        $result = $api->lookup('0444444444');

        $this->assertEquals("Taxi 444 AG", $result->title);
    }

}