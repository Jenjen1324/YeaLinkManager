<?php $this->layout('html/main') ?>

<?php $this->start('body') ?>

<table>
    <tr>
        <th>Numbers</th>
        <th>Name</th>
    </tr>

    <?php foreach ($data as $entry): ?>
    <tr>
        <td>
            <?php foreach ($entry['numbers'] as $number): ?>
            <?= $this->e($number) ?><br>
            <?php endforeach; ?>
        </td>
        <td>
            <?= $this->e($entry['name']) ?>
        </td>
    </tr>
    <?php endforeach; ?>
</table>

<?php $this->end() ?>
