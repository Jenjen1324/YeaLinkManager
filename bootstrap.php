<?php
/**
 * Created by PhpStorm.
 * User: jvogler
 * Date: 12.03.2019
 * Time: 14:14
 */

require_once __DIR__ . '/vendor/autoload.php';

$containerBuilder = new DI\ContainerBuilder();

$containerBuilder->useAnnotations(true);
$containerBuilder->addDefinitions(__DIR__ . '/container.php');
$container = $containerBuilder->build();



return $container;