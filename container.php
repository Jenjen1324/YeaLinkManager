<?php
/**
 * Created by PhpStorm.
 * User: jvogler
 * Date: 12.03.2019
 * Time: 14:14
 */

require_once __DIR__ . '/config.php';

use Doctrine\Common\Cache\ApcuCache;
use Doctrine\Common\Cache\ArrayCache;
use Doctrine\Common\Cache\CacheProvider;
use Doctrine\ORM\EntityManager;
use Psr\Log\LoggerInterface;
use Psr\Log\LogLevel;
use Symfony\Component\Console\Formatter\OutputFormatter;
use Symfony\Component\Console\Formatter\OutputFormatterStyle;
use Symfony\Component\Console\Logger\ConsoleLogger;
use Symfony\Component\Console\Output\ConsoleOutput;
use YeaLinkManager\Scheduler;

return [

    Config::class => function () {
        return new Config();
    },

    LoggerInterface::class => function (Config $config) {
        if ($config->devMode) {

            $outputFormatter = new OutputFormatter(true);
            $outputFormatter->setStyle("emergency", new OutputFormatterStyle("white", "red"));
            $outputFormatter->setStyle("error", new OutputFormatterStyle("red"));
            $outputFormatter->setStyle("warning", new OutputFormatterStyle("yellow"));
            $outputFormatter->setStyle("notice", new OutputFormatterStyle("green"));
            $outputFormatter->setStyle("info", new OutputFormatterStyle("blue"));
            $outputFormatter->setStyle("debug", new OutputFormatterStyle("magenta"));

            $formatLevelMap = array(
                LogLevel::EMERGENCY => 'emergency',
                LogLevel::ALERT     => 'error',
                LogLevel::CRITICAL  => 'error',
                LogLevel::ERROR     => 'error',
                LogLevel::WARNING   => 'warning',
                LogLevel::NOTICE    => 'notice',
                LogLevel::INFO      => 'info',
                LogLevel::DEBUG     => 'debug',
            );

            $consoleLogger = new ConsoleLogger(new ConsoleOutput(ConsoleOutput::VERBOSITY_DEBUG, true, $outputFormatter), [], $formatLevelMap);

            return new \YeaLinkManager\Logger\FormattedLogWrapper($consoleLogger);
        } else {
            return new \Psr\Log\NullLogger();
            //return new \YeaLinkManager\Logger\ErrorLogger();
        }
    },

    'cache.fast' => function (\Config $config) {
        if ($config->devMode) {
            return new ArrayCache();
        }
        else {
            return new ApcuCache();
        }
    },

    EntityManager::class => DI\factory(function (CacheProvider $cache, \Config $config) {
        $cache->setNamespace(EntityManager::class . '__Annotations');

        $reader = new \Doctrine\Common\Annotations\AnnotationReader();
        $driver = new \Doctrine\ORM\Mapping\Driver\AnnotationDriver($reader, [__DIR__ . '/src/Entities']);

        $doctrineConfig = \Doctrine\ORM\Tools\Setup::createAnnotationMetadataConfiguration([__DIR__ . '/src/Entities'], true);
        $doctrineConfig->setMetadataCacheImpl($cache);
        $doctrineConfig->setQueryCacheImpl($cache);
        $doctrineConfig->setMetadataDriverImpl($driver);

        return EntityManager::create($config->dbParams, $doctrineConfig);

    })->parameter('cache', DI\get('cache.fast')),

    Scheduler::class => DI\create(Scheduler::class),

    \League\Plates\Engine::class => function () {
        return new \League\Plates\Engine(__DIR__ . '/templates');
    }

];